import {
  MailOutline,
  KeyOutline,
  TeamOutline,
  UserAddOutline,
  LogoutOutline,
  SearchOutline,
  MenuFoldOutline,
  MenuUnfoldOutline,
  FilterOutline,
  UserOutline,
  DollarOutline,
} from '@ant-design/icons-angular/icons';
import { IconDefinition } from '@ant-design/icons-angular';

export const icons: IconDefinition[] = [
  MailOutline,
  KeyOutline,
  TeamOutline,
  UserAddOutline,
  LogoutOutline,
  SearchOutline,
  MenuFoldOutline,
  MenuUnfoldOutline,
  FilterOutline,
  UserOutline,
  DollarOutline,
];
