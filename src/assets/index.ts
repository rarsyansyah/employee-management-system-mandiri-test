import Employees from './employees.json';
import Groups from './groups.json';
import { icons as Icons } from './icons';

export { Employees, Groups, Icons };
