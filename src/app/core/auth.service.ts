import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  login(username?: string, password?: string): boolean {
    if (username === 'admin@mandiri.com' && password === 'admin123') {
      if (typeof localStorage !== 'undefined') {
        localStorage.setItem('isLogin', JSON.stringify(true));
      }
      return true;
    }

    return false;
  }

  logout(): void {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem('isLogin');
      localStorage.removeItem('employees');
    }
  }

  getIsAuthenticated(): boolean {
    if (typeof localStorage !== 'undefined') {
      const isLogin = localStorage.getItem('isLogin');
      return isLogin !== null;
    }

    return false;
  }
}
