import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../core/auth.service';

export const authGuard: CanActivateFn = (route, state) => {
  return inject(AuthService).getIsAuthenticated()
    ? true
    : inject(Router).navigateByUrl('/login');
};
