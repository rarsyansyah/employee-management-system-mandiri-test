import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { inject } from '@angular/core';

export const unauthGuard: CanActivateFn = (route, state) => {
  return inject(AuthService).getIsAuthenticated()
    ? inject(Router).navigateByUrl('/employees')
    : true;
};
