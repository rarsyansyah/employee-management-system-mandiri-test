import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Employee } from '../../interfaces/employee';

@Component({
  selector: 'employee-list-mobile',
  templateUrl: './employee-list-mobile.component.html',
  styleUrl: './employee-list-mobile.component.scss',
})
export class EmployeeListMobileComponent {
  @Input({ required: true }) loading!: boolean;
  @Input({ required: true }) employees!: Employee[];
  @Input({ required: true }) total!: number;
  @Input({ required: true }) pageIndex!: number;
  @Input({ required: true }) pageSize!: number;

  @Output() onShowDetail = new EventEmitter<number>();
  @Output() onShowEdit = new EventEmitter<number>();
  @Output() onDelete = new EventEmitter<number>();
  @Output() onPageIndexChange = new EventEmitter<number>();
  @Output() onPageSizeChange = new EventEmitter<number>();

  showDetailModal(id: number): void {
    this.onShowDetail.emit(id);
  }

  showEditModal(id: number): void {
    this.onShowEdit.emit(id);
  }

  delete(id: number): void {
    this.onDelete.emit(id);
  }

  handlePageIndexChange(index: number): void {
    this.onPageIndexChange.emit(index);
  }

  handlePageSizeChange(size: number): void {
    this.onPageSizeChange.emit(size);
  }
}
