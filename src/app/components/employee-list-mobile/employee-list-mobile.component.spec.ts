import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListMobileComponent } from './employee-list-mobile.component';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzButtonModule } from 'ng-zorro-antd/button';

describe('EmployeeListMobileComponent', () => {
  let component: EmployeeListMobileComponent;
  let fixture: ComponentFixture<EmployeeListMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeeListMobileComponent],
      imports: [NzListModule, NzPaginationModule, NzButtonModule],
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeeListMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
