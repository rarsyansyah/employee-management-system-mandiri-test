import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListTableComponent } from './employee-list-table.component';
import { NzTableModule } from 'ng-zorro-antd/table';

describe('EmployeeListTableComponent', () => {
  let component: EmployeeListTableComponent;
  let fixture: ComponentFixture<EmployeeListTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeeListTableComponent],
      imports: [NzTableModule],
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeeListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
