import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Employee } from '../../interfaces/employee';
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
  selector: 'employee-list-table',
  templateUrl: './employee-list-table.component.html',
  styleUrl: './employee-list-table.component.scss',
})
export class EmployeeListTableComponent {
  @Input({ required: true }) loading!: boolean;
  @Input({ required: true }) employees!: Employee[];
  @Input({ required: true }) total!: number;
  @Input({ required: true }) pageIndex!: number;
  @Input({ required: true }) pageSize!: number;

  @Output() onShowDetail = new EventEmitter<number>();
  @Output() onShowEdit = new EventEmitter<number>();
  @Output() onDelete = new EventEmitter<number>();
  @Output() onPageIndexChange = new EventEmitter<number>();
  @Output() onPageSizeChange = new EventEmitter<number>();
  @Output() onQueryParamsChange = new EventEmitter<NzTableQueryParams>();

  showDetailModal(id: number): void {
    this.onShowDetail.emit(id);
    console.info({ id });
  }

  showEditModal(id: number): void {
    this.onShowEdit.emit(id);
  }

  delete(id: number): void {
    this.onDelete.emit(id);
  }

  handlePageIndexChange(index: number): void {
    this.onPageIndexChange.emit(index);
  }

  handlePageSizeChange(size: number): void {
    this.onPageSizeChange.emit(size);
  }

  handleQueryParamsChange(params: NzTableQueryParams): void {
    this.onQueryParamsChange.emit(params);
  }
}
