import { Component } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrl: './auth-layout.component.scss',
})
export class AuthLayoutComponent {
  isCollapsed = true;

  constructor(private authService: AuthService) {}

  onLogout(): void {
    const confirmation = confirm('Yakin ingin keluar ?');

    if (confirmation) {
      this.authService.logout();
      window.location.href = '/login';
    }
  }
}
