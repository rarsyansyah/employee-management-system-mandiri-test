import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Pagination } from '../../interfaces/pagination';

@Component({
  selector: 'employee-list-search',
  templateUrl: './employee-list-search.component.html',
  styleUrl: './employee-list-search.component.scss',
})
export class EmployeeListSearchComponent {
  @Input() sort?: Pagination['sort'];
  @Input() search?: string;

  @Output() onSearch = new EventEmitter<string>();
  @Output() onFilter = new EventEmitter<Pagination['sort']>();

  handleSearch(text: string): void {
    this.onSearch.emit(text);
  }

  handleFilter(value: 'asc' | 'desc'): void {
    this.onFilter.emit(value);
  }
}
