import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListSearchComponent } from './employee-list-search.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { Icons } from '../../../assets';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('EmployeeListSearchComponent', () => {
  let component: EmployeeListSearchComponent;
  let fixture: ComponentFixture<EmployeeListSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeeListSearchComponent],
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        NzSelectModule,
        NzInputModule,
        NzIconModule.forChild(Icons),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeeListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
