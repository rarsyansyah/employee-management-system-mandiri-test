export interface Pagination {
  sort?: 'asc' | 'desc';
  search?: string;
}
