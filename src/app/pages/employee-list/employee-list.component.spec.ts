import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListComponent } from './employee-list.component';
import { EmployeeListSearchComponent } from '../../components/employee-list-search/employee-list-search.component';
import { EmployeeListTableComponent } from '../../components/employee-list-table/employee-list-table.component';
import { EmployeeListMobileComponent } from '../../components/employee-list-mobile/employee-list-mobile.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { Icons } from '../../../assets';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzTableModule } from 'ng-zorro-antd/table';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('EmployeeListComponent', () => {
  let component: EmployeeListComponent;
  let fixture: ComponentFixture<EmployeeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        EmployeeListComponent,
        EmployeeListSearchComponent,
        EmployeeListTableComponent,
        EmployeeListMobileComponent,
      ],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        NzSelectModule,
        NzInputModule,
        NzListModule,
        NzButtonModule,
        NzPaginationModule,
        NzTableModule,
        NzIconModule.forChild(Icons),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
