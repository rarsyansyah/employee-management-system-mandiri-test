import { Component, OnInit, OnDestroy } from '@angular/core';
import { Employee } from '../../interfaces/employee';
import { Pagination } from '../../interfaces/pagination';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { EmployeeService } from '../../core/employee.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.scss',
})
export class EmployeeListComponent implements OnInit, OnDestroy {
  loading = true;
  pageSize = 10;
  pageIndex = 1;
  pagination!: Pagination;

  private searchSubject = new Subject<string>();
  private readonly debounceTimeMs = 300;

  employees!: Employee[];

  constructor(
    private employeeService: EmployeeService,
    private notificationService: NzNotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.searchSubject
      .pipe(debounceTime(this.debounceTimeMs))
      .subscribe((value) => {
        this.searchAction(value);
      });

    this.pagination = this.employeeService.getPagination();
    this.fetchEmployees();
  }

  ngOnDestroy(): void {
    this.searchSubject.complete();
  }

  fetchEmployees(): void {
    this.loading = true;
    this.employees = this.employeeService.getAll();
    this.loading = false;
  }

  getEmployees(): Employee[] {
    const filtered = this.employees.filter((item) => {
      if (!this.pagination.search) {
        return true;
      }

      return (
        item.firstName
          .toLowerCase()
          .includes(this.pagination.search.toLowerCase()) ||
        item.lastName
          ?.toLowerCase()
          .includes(this.pagination.search.toLowerCase())
      );
    });

    if (this.pagination.sort === 'asc')
      return filtered.sort((a, b) => a.firstName.localeCompare(b.firstName));

    if (this.pagination.sort === 'desc')
      return filtered.sort((a, b) => b.firstName.localeCompare(a.firstName));

    return filtered;
  }

  filteredEmployees(): Employee[] {
    return this.getEmployees().slice(
      (this.pageIndex - 1) * this.pageSize,
      this.pageIndex * this.pageSize
    );
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
  }

  onPageIndexChange(index: number): void {
    this.pageIndex = index;
  }

  onPageSizeChange(size: number): void {
    this.pageSize = size;
  }

  onSearch(text: string): void {
    this.searchSubject.next(text);
  }

  searchAction(text: string): void {
    this.pagination.search = text;

    this.employeeService.setPagination({
      search: text,
      sort: this.pagination.sort,
    });
  }

  onFilter(value: Pagination['sort']): void {
    this.pagination.sort = value;

    this.employeeService.setPagination({
      search: this.pagination.search,
      sort: value,
    });
  }

  onDelete(id: number): void {
    this.notificationService.error('Hapus', 'ID ' + id);
  }

  onEdit(id: number): void {
    this.notificationService.warning('Edit', 'ID ' + id);
  }

  onDetail(id: number): void {
    this.router.navigate(['employees', id]);
  }
}
