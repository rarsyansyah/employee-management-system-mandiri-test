import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../core/employee.service';
import { Employee } from '../../interfaces/employee';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrl: './employee-detail.component.scss',
})
export class EmployeeDetailComponent implements OnInit {
  loading = true;
  employeeId?: number;
  employee?: Employee;

  constructor(
    private employeeService: EmployeeService,
    private activeRoute: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.fetchEmployee();
  }

  fetchEmployee(): void {
    const employeeId = Number(this.activeRoute.snapshot.params['id']);

    this.loading = true;
    this.employee = this.employeeService.getOne(employeeId);
    this.loading = false;
  }

  onBack(): void {
    this.location.back();
  }
}
