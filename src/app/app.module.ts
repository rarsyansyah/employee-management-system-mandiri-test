import { NgModule } from '@angular/core';
import {
  BrowserModule,
  provideClientHydration,
} from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import id from '@angular/common/locales/id';
import { provideNzI18n, id_ID } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
registerLocaleData(id);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeFormComponent } from './pages/employee-form/employee-form.component';
import { EmployeeListComponent } from './pages/employee-list/employee-list.component';
import { LoginComponent } from './pages/login/login.component';
import { EmployeeDetailComponent } from './pages/employee-detail/employee-detail.component';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzRadioModule } from 'ng-zorro-antd/radio';

import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { EmployeeListSearchComponent } from './components/employee-list-search/employee-list-search.component';
import { EmployeeListMobileComponent } from './components/employee-list-mobile/employee-list-mobile.component';
import { EmployeeListTableComponent } from './components/employee-list-table/employee-list-table.component';
import { Icons } from '../assets';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeFormComponent,
    EmployeeListComponent,
    LoginComponent,
    EmployeeDetailComponent,
    AuthLayoutComponent,
    EmployeeListSearchComponent,
    EmployeeListMobileComponent,
    EmployeeListTableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzInputModule,
    NzImageModule,
    NzLayoutModule,
    NzSelectModule,
    NzTableModule,
    NzListModule,
    NzPaginationModule,
    NzMenuModule,
    NzNotificationModule,
    NzDatePickerModule,
    NzRadioModule,
    NzIconModule.forChild(Icons),
  ],
  providers: [provideClientHydration(), provideNzI18n(id_ID)],
  bootstrap: [AppComponent],
})
export class AppModule {}
